module.exports = {
    randomString: function() {
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let result = '';
        for (let i = 0; i < 15; i++) {
            result += chars[Math.floor(Math.random() * chars.length)];
        }
        return result;
    }
}


const fs = require('fs');
const { randomString } = require('./helpers/stringHelpers.js');


module.exports = (req, res) => {
    switch(req.url) {
        case '/':
            var page = fs.readFileSync('./template/index.html', 'utf-8');
            res.writeHead(200, { 'Content-Type': 'text/html' })
            res.end(page);
            break;
        case '/events':
            res.writeHead(200, {
                'Content-Type' : 'text/event-stream',
                'Cache-Control' : 'no-cache',
                'Connection' : 'keep-alive'
                });

                var sseId = (new Date()).toLocaleTimeString();
                res.write('id: ' + sseId + '\n');
                function sendData() {
                    setTimeout(() => {
                        res.write('data:' + JSON.stringify({
                            value: randomString(),
                            time: (new Date()).toLocaleTimeString()
                        }) + '\n\n');
                        sendData()
                    }, (Math.floor(Math.random() * 7) + 3) * 1000)
                }
                sendData()
            break;
    }
}